import java.util.Stack;


public class MoteurRPN {

	Stack<Double> operandes =new Stack<Double>();
	
	
	public void addOperande( double x){
		
		operandes.add(x);
	}
	public double AppliquerOperation(Operation o){
		
		double x ,y;
		x = operandes.pop();
		y = operandes.pop();
		operandes.add(o.eval(x, y));
		return o.eval(x, y);
	}
	@Override
	public String toString() {
		return "MoteurRPN [operandes=" + operandes + "]";
	}
	
	
}
